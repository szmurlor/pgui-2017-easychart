package logic;

import java.util.Arrays;
import java.util.List;

/**
 * Created by szmurlor on 2017-06-03.
 */
public class PlotData {
    private double[][] data = null;
    private int rows;
    private int cols;

    public PlotData() {
        resize(2,2);
    }

    public void resize(int rows, int cols) {
        double[][] newdata = new double[rows][cols];

        if (data != null) {
            for (int r = 0; r < this.rows && r < rows; r++) {
                for (int c = 0; c < this.cols && c < cols; c++) {
                    newdata[r][c] = data[r][c];
                }
            }
        }

        this.rows = rows;
        this.cols = cols;
        this.data = newdata;
    }

    public void set(int r, int c, double v) {
        data[r][c] = v;
    }

    public double get(int r, int c) {
        return data[r][c];
    }

    public List<double[]> getList() {
        return Arrays.asList(data);
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }
}
