package pw.edu.pl;

import logic.PlotData;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	private final ModalWindow modal;
	private final WebMarkupContainer contTable;

	public HomePage(final PageParameters parameters) {
		super(parameters);

		add(modal = new ModalWindow("modal"));

		add(new AjaxFallbackLink<Void>("cmdSetRows") {
			@Override
			public void onClick(AjaxRequestTarget target) {
				modal.setContent(new FragSetRows());
				modal.show(target);
			}
		});

		add(contTable = new WebMarkupContainer("contTable"));
		contTable.setOutputMarkupId(true);

		WicketApplication app = (WicketApplication) getApplication();
		contTable.add( new ListView<double[]>("contRows",  new PropertyModel<List<double[]>>(app.getPlotData(), "list")){ // app.getPlotData().getList()) {

			@Override
			protected void populateItem(final ListItem<double[]> itemRows) {
				List<Double> columns = new LinkedList<>();
				for (double d: itemRows.getModelObject()) {
					columns.add(d);
				}

				itemRows.add(new ListView<Double>("contCols", columns) {
					@Override
					protected void populateItem(final ListItem<Double> itemCols) {
						// listItem.add( new Label("value", listItem.getModelObject()));
						itemCols.add( new AjaxEditableLabel<Double>("value", Model.of(itemCols.getModelObject())){
							@Override
							protected void onSubmit(AjaxRequestTarget target) {
								super.onSubmit(target);
								int r = itemRows.getIndex();
								int c = itemCols.getIndex();
								getPlotData().set(r,c, getModelObject());
							}
						});
					}
				});
			}
		});

    }

    protected class FragSetRows extends Fragment {
		protected int rows;
		protected int cols;

		public FragSetRows() {
			super(modal.getContentId(), "fragSetRows", HomePage.this);

			rows = getPlotData().getRows();
			cols = getPlotData().getCols();

			Form<Void> form;
			add( form = new Form<Void>("form"));
			form.add(new TextField<Integer>("txtRows", new PropertyModel<Integer>(this, "rows")));
			form.add(new TextField<Integer>("txtCols", new PropertyModel<Integer>(this, "cols")));

			form.add(new AjaxFallbackLink<Void>("cmdCancel") {
				@Override
				public void onClick(AjaxRequestTarget target) {
					modal.close(target);
				}
			});
			form.add(new AjaxFallbackButton("cmdSave", form) {
				@Override
				protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
					super.onSubmit(target, form);

					getPlotData().resize(rows, cols);
					modal.close(target);

					target.add(contTable);
				}
			});
		}
	}

	private PlotData getPlotData() {
		return ((WicketApplication) getApplication()).getPlotData();
	}
}
