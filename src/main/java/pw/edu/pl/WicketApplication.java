package pw.edu.pl;

import logic.PlotData;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;

/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 * 
 * @see pw.edu.pl.Start#main(String[])
 */
public class WicketApplication extends WebApplication
{
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();

		// add your configuration here
	}

	private PlotData _plotData;
	public PlotData getPlotData() {
		if (_plotData == null) {
			_plotData = new PlotData();
			bootstrapData(_plotData);
		}
		return _plotData;
	}

	private void bootstrapData(PlotData pd) {
		pd.resize(2,2);
		pd.set(0,0, 1);
		pd.set(0,1, 2);
		pd.set(1,0, 3);
		pd.set(1,1, 4);
	}
}
